# Side Project 1 - Communication Interfaces

## Table of Contents
- [Side Project 1 - Communication Interfaces](#side-project-1---communication-interfaces)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
    - [IOCTL (Input/Output Control)](#ioctl-inputoutput-control)
    - [Procfs (Process Filesystem) \& Sysfs](#procfs-process-filesystem--sysfs)
    - [Configfs](#configfs)
    - [Debugfs](#debugfs)
    - [Sysctl](#sysctl)
    - [UDP Sockets](#udp-sockets)
    - [Netlink Sockets](#netlink-sockets)
  - [Conclusion Table](#conclusion-table)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

There differents way to communicate between the Userspace and Kernel Space:
  - [IOCTL](./ioctl.c)
  - [Procfs](./procfs.c)
  - [Sysfs](./sysfs.c)
  - Configfs
  - Debugfs
  - Sysctl
  - UDP Sockets
  - Netlink Sockets

### IOCTL (Input/Output Control)

  Low-Level Hardware Access (IOCTL)
  
  When you are writing device drivers or need to have a direct line to hardware, IOCTL commands are often necessary to perform operations that userspace applications cannot perform directly.
  
  How to call IOCTL in user space?

  ```
  #define WR_VALUE _IOW('a','a',int32_t*)
  #define RD_VALUE _IOR('a','b',int32_t*)
  ...
  ioctl(fd, WR_VALUE, (int32_t*) &in_value); 
  ioctl(fd, RD_VALUE, (int32_t*) &out_value);
  ```

  After that, IOCTL function will be called

  ```
  static long demo_ioctl(struct file *file, unsigned int cmd, unsigned long arg){
          switch(cmd) {
                  case WR_VALUE:
                          if( copy_from_user(&value ,(int32_t*) arg, sizeof(value))){
                                  pr_err("Data Write : Err!\n");
                          }
                          pr_info("Value = %d\n", value);
                          break;
                  case RD_VALUE:
                          if( copy_to_user((int32_t*) arg, &value, sizeof(value))){
                                  pr_err("Data Read : Err!\n");
                          }
                          break;
                  default:
                          pr_info("Default\n");
                          break;
          }
          return 0;
  }
  ```

### Procfs (Process Filesystem) & Sysfs

  Dynamic Monitoring (Procfs/Sysfs)

  If you need to monitor or change the state of running processes, CPU statistics, memory usage, and so forth, Procfs and Sysfs can provide that info dynamically, directly from the kernel.

  Sysfs is the commonly used method to export system information from the kernel space to the user space for specific devices. The SysFS is tied to the device driver model of the kernel. The procfs is used to export the process-specific information and the debugfs is used to export the debug information by the developer.

  Create procfs directory

  ```
  parent = proc_mkdir("demo",NULL);
  ```
  
  Create procfs entry

  ```
  proc_create("demo_proc", 0666, parent, &proc_fops);
  ```

  Read the procfs variable by `cat`

  ```
  cat /proc/demo/demo_proc
  ```
  
  Create a directory in /sys

  ```
  kobj_ref = kobject_create_and_add("demo_sysfs",kernel_kobj);

  ```
  
  Create Sysfs file

  ```
  struct kobject *kobj_ref;
  ...
  if(sysfs_create_file(kobj_ref,&demo_attr.attr)){
                pr_err("Cannot create sysfs file......\n");
                goto r_sysfs;
        }
        pr_info("Device Driver Insert...Done!!!\n");
        return 0;
  ```
  Check the sysfs file by `ls`

  ```
  ls -l /sys/kernel/demo_sysfs
  ```

### Configfs

  Configuration and Tuning (Sysctl, Configfs)

  Some kernel parameters need to be modified on-the-fly for tuning system performance or behavior without a reboot. Sysctl and Configfs allow this kind of dynamic interaction.

### Debugfs
  
  Debugging (Debugfs)

  During the development or debugging phases, it's often necessary to pull internal states from the kernel or to change kernel variables to test different conditions. Debugfs is a powerful tool for such tasks.

### Sysctl

  Fine-grained Control (Configfs, Sysfs)
  When the pre-defined interfaces like Procfs or IOCTL are not enough, or too cumbersome, fine-grained control over kernel objects can be achieved using Sysfs or Configfs.

### UDP Sockets

  Standard Network Communication (UDP Sockets)

  If you are doing network programming that needs to interact with the kernel, using standard UDP sockets might be appropriate. This is not specific to kernel-userspace communication but can serve the purpose in some scenarios.

### Netlink Sockets

  Complex Communication (Netlink Sockets)

  For tasks like system networking, where complex packets of data need to be passed to and from userspace and kernel space, Netlink sockets are essential. They are designed for high-bandwidth, high-complexity tasks.

## Conclusion Table

| Method       | Purpose         | Flexibility    | Performance    | Ease of Use    | Security       |
|--------------|-----------------|----------------|----------------|----------------|----------------|
| IOCTL        | Device Drivers  | High           | Good           | Moderate       | Varies         |
| Procfs       | General Info    | Moderate       | Moderate       | Easy           | Risky          |
| Sysfs        | Kernel Objects  | Good           | Moderate       | Moderate       | Safer          |
| Configfs     | Kernel Objects  | High           | Moderate       | Moderate       | Safer          |
| Debugfs      | Debugging       | High           | Poor           | Easy           | Insecure       |
| Sysctl       | Kernel Params   | Low            | Good           | Easy           | Controlled     |
| UDP Sockets  | Networking      | High           | Less Efficient | Moderate       | Network-based  |
| Netlink      | Networking      | High           | Good           | Complex        | Secure         |

Choose a method based on your specific needs, whether it's flexibility, performance, or ease of use.

## Rivision history : v1.0
