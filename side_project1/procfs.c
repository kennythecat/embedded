#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/slab.h>                 //kmalloc()
#include<linux/uaccess.h>              //copy_to/from_user()
#include <linux/ioctl.h>
#include<linux/proc_fs.h>
#include <linux/err.h>

#define LINUX_KERNEL_VERSION  510
 
int32_t value = 0;
char demo_array[20]="try_proc_array\n";
static int len = 1;
 
static struct proc_dir_entry *parent;


static int      __init demo_driver_init(void);
static void     __exit demo_driver_exit(void);
 
static int      open_proc(struct inode *inode, struct file *file);
static int      release_proc(struct inode *inode, struct file *file);
static ssize_t  read_proc(struct file *filp, char __user *buffer, size_t length,loff_t * offset);
static ssize_t  write_proc(struct file *filp, const char *buff, size_t len, loff_t * off);

static struct proc_ops proc_fops = {
        .proc_open = open_proc,
        .proc_read = read_proc,
        .proc_write = write_proc,
        .proc_release = release_proc
};


static int open_proc(struct inode *inode, struct file *file)
{
    pr_info("proc file opend.....\t");
    return 0;
}


static int release_proc(struct inode *inode, struct file *file)
{
    pr_info("proc file released.....\n");
    return 0;
}

static ssize_t read_proc(struct file *filp, char __user *buffer, size_t length,loff_t * offset)
{
    pr_info("proc file read.....\n");
    if(len){
        len=0;
    }
    else{
        len=1;
        return 0;
    }
    
    if( copy_to_user(buffer,demo_array,20) ){
        pr_err("Data Send : Err!\n");
    }
 
    return length;
}

static ssize_t write_proc(struct file *filp, const char *buff, size_t len, loff_t * off)
{
    pr_info("proc file wrote.....\n");
    
    if( copy_from_user(demo_array,buff,len) )
    {
        pr_err("Data Write : Err!\n");
    }
    
    return len;
}
 
static int __init demo_driver_init(void)
{
        /*Create proc directory. It will create a directory under "/proc" */
        parent = proc_mkdir("demo",NULL);
        
        if( parent == NULL )
        {
            pr_info("Error creating proc entry");
            goto r_device;
        }
        
        /*Creating Proc entry under "/proc/demo/" */
        proc_create("demo_proc", 0666, parent, &proc_fops);
 
        pr_info("Device Driver Insert...Done!!!\n");
        return 0;
 
}
 
static void __exit demo_driver_exit(void)
{       
        /* remove complete /proc/demo */
        proc_remove(parent);
        
}
 
module_init(demo_driver_init);
module_exit(demo_driver_exit);
 
MODULE_LICENSE("GPL");
