#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/slab.h>                 //kmalloc()
#include<linux/uaccess.h>              //copy_to/from_user()
#include<linux/sysfs.h> 
#include<linux/kobject.h> 
#include <linux/err.h>
 
struct kobject *kobj_ref;

static int      __init demo_driver_init(void);
static void     __exit demo_driver_exit(void);
 
 
static ssize_t  sysfs_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf);
static ssize_t  sysfs_store(struct kobject *kobj, struct kobj_attribute *attr,const char *buf, size_t count);

struct kobj_attribute demo_attr = __ATTR(demo_value, 0660, sysfs_show, sysfs_store);


static ssize_t sysfs_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
        pr_info("Sysfs - Read!!!\n");
        return sprintf(buf, "%d", demo_value);
}

static ssize_t sysfs_store(struct kobject *kobj, struct kobj_attribute *attr,const char *buf, size_t count)
{
        pr_info("Sysfs - Write!!!\n");
        sscanf(buf,"%d",&demo_value);
        return count;
}

 
static int __init demo_driver_init(void)
{
        /*Creating a directory in /sys/kernel/ */
        kobj_ref = kobject_create_and_add("demo_sysfs",kernel_kobj);
 
        /*Creating sysfs file for demo_value*/
        if(sysfs_create_file(kobj_ref,&demo_attr.attr)){
                pr_err("Cannot create sysfs file......\n");
                goto r_sysfs;
        }
        pr_info("Device Driver Insert...Done!!!\n");
        return 0;
 
r_sysfs:
        kobject_put(kobj_ref); 
        sysfs_remove_file(kernel_kobj, &demo_attr.attr);
}

static void __exit demo_driver_exit(void)
{
        kobject_put(kobj_ref); 
        sysfs_remove_file(kernel_kobj, &demo_attr.attr);
        pr_info("Device Driver Remove...Done!!!\n");
}
 
module_init(demo_driver_init);
module_exit(demo_driver_exit);
 
MODULE_LICENSE("GPL");
