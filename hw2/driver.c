#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/slab.h>                
#include<linux/uaccess.h>              
#include <linux/err.h>
 

#define mem_size        1024           
 
dev_t dev = 0;
static struct class *dev_class;
static struct cdev demo_cdev;
uint8_t *kernel_buffer;

// 初始化 Function Prototypes
static int      __init demo_driver_init(void);
static void     __exit demo_driver_exit(void);
static int      demo_open(struct inode *inode, struct file *file);
static int      demo_release(struct inode *inode, struct file *file);
static ssize_t  demo_read(struct file *filp, char __user *buf, size_t len,loff_t * off);
static ssize_t  demo_write(struct file *filp, const char *buf, size_t len, loff_t * off);

// File Operations structure
static struct file_operations fops =
{
        .owner          = THIS_MODULE,
        .read           = demo_read,
        .write          = demo_write,
        .open           = demo_open,
        .release        = demo_release,
};

// 當開起device時會呼叫此Function
static int demo_open(struct inode *inode, struct file *file)
{
        pr_info("Device File Opened !\n");
        return 0;
}

// 當關閉device時會呼叫此Function
static int demo_release(struct inode *inode, struct file *file)
{
        pr_info("Device File Closed !\n");
        return 0;
}

// 當讀取device時會呼叫此Function
static ssize_t demo_read(struct file *filp, char __user *buf, size_t len, loff_t *off)
{
        // 將資料從Kernel space複製到User Space
        if( copy_to_user(buf, kernel_buffer, mem_size) )
        {
                pr_err("Data Read : Error !\n");
        }
        pr_info("Data Read !\n");
        return mem_size;
}

// 當寫入device時會呼叫此Function
static ssize_t demo_write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
        // 將資料從Kernel space複製到User Space
        if( copy_from_user(kernel_buffer, buf, len) )
        {
                pr_err("Data Write Error !\n");
        }
        pr_info("Data Write !\n");
        return len;
}

static int __init demo_driver_init(void)
{
        //與講義不同 我使用動態分配Major與Minor
        if((alloc_chrdev_region(&dev, 0, 1, "demo_Dev")) <0){
                pr_info("Cannot allocate major number\n");
                return -1;
        }
        pr_info("Major = %d Minor = %d \n",MAJOR(dev), MINOR(dev));
        
        //初始化 cdev structure
        cdev_init(&demo_cdev,&fops);
 
        //將character Device加入系統中 若失敗會將class關閉
        if((cdev_add(&demo_cdev,dev,1)) < 0){
            pr_info("Cannot add the device to the system\n");
            goto r_class;
        }

        //建構 struct class 若失敗會將class關閉
        if(IS_ERR(dev_class = class_create(THIS_MODULE,"demo_class"))){
            pr_info("Cannot create the struct class\n");
            goto r_class;
        }
        
        //建構 Device 若失敗會註銷device
        if(IS_ERR(device_create(dev_class,NULL,dev,NULL,"demo_device"))){
            pr_info("Cannot create the Device 1\n");
            goto r_device;
        }
        
        //配置記憶體給device 若失敗會註銷device
        if((kernel_buffer = kmalloc(mem_size , GFP_KERNEL)) == 0){
            pr_info("Cannot allocate memory in kernel\n");
            goto r_device;
        }
                
        pr_info("Device Driver INIT !\n");
        return 0;
//關閉class
r_device:
        class_destroy(dev_class);
//註銷device
r_class:
        unregister_chrdev_region(dev,1);
        return -1;
}

//在退出module時將佔用空間釋放,且關閉class與註dDevice
static void __exit demo_driver_exit(void)
{
        kfree(kernel_buffer);
        device_destroy(dev_class,dev);
        class_destroy(dev_class);
        cdev_del(&demo_cdev);
        unregister_chrdev_region(dev, 1);
        pr_info("Device Driver EXIT !\n");
}
 
module_init(demo_driver_init);
module_exit(demo_driver_exit);
 
MODULE_LICENSE("GPL");