# hw2 - Generic Driver

## Table of Contents
- [hw2 - Generic Driver](#hw2---generic-driver)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [How to run](#how-to-run)
  - [Part 1](#part-1)
  - [Part 2](#part-2)
  - [Common commands](#common-commands)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

Extend kernel module into a character driver and Write a C based program to access the driver

## How to run
Install build-essential package​

```
sudo apt-get install build-essential
```

Mount the module

```
sudo insmod hello_world.ko
```

Check if it’s mounted

```
sudo lsmod | grep "hello_world”
```

Unmount the module 

```
sudo rmmod hello_world
```

Go to dmesg to find the print message

```
dmesg
```

## Part 1

 Extend kernel module into a [character driver](./driver.c)​

  - Need to support open/close/read/write​
  - Implement above functions to access a backend file​
  
    ```
    static int demo_open(struct inode *inode, struct file *file){
    
    }

    static int demo_release(struct inode *inode, struct file *file){

    }

    static ssize_t demo_read(struct file *filp, char __user *buf, size_t len, loff_t *off){

    }

    static ssize_t demo_write(struct file *filp, const char __user *buf, size_t len, loff_t *off){

    }
    ```

## Part 2

 Write a C based program to access the driver​

  - Note that C program are in user space, Just like any other regular program you wrote​
  - In Linux, you can write a makefile to compile you [C program​](./driver.c)

## Common commands

```
sed -n '/^Character/, /^$/ { /^$/ !p }' /proc/devices

cat /proc/devices

sudo mknod -m 666 /dev/demo c <major> <minor>
```

## Rivision history : v1.0
