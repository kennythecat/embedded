#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
static char buf_o[26];
static char buf_i[26] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
main()
{
	int fd, ret;

	fd = open("/dev/demo_device",O_RDWR);
	if(fd<0) perror("open fail. If Permission denied, make sure use -> sudo \n");
	printf("open file \n");
	
	ret = write(fd, buf_i, sizeof(buf_i));
	if(ret<0) perror("write fail \n");
	printf("Write file \n");
	
	ret = read(fd, buf_o, sizeof(buf_o));
	if(ret<0) perror("read fail \n");
	printf("Read file : %s\n", buf_o);
	close(fd);
}