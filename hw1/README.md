# hw1 - Read/Write In Linux Kernel


## Table of Contents
- [hw1 - Read/Write In Linux Kernel](#hw1---readwrite-in-linux-kernel)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
  - [How to run](#how-to-run)
  - [Part 1](#part-1)
  - [Part 2](#part-2)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

Learn how to write a kernel module and read files in a kernel module

## How to run
Install build-essential package​

```
sudo apt-get install build-essential
```

Mount the module

```
sudo insmod hello_world.ko
```

Check if it’s mounted

```
sudo lsmod | grep "hello_world”
```

Unmount the module 

```
sudo rmmod hello_world
```

Go to dmesg to find the print message

```
dmesg
```

## Part 1

 Learn how to write a kernel module and Print "Hello_world!"

  1. Install build-essential package
  2. Create [hello_world.c](./hello_world.c) & [Makefile](./Makefile)  in that folder
  3. Compile your code with make

## Part 2

 Learn how to read files in a kernel module

1. Create [fileIO.c](./fileIO.c) & [Makefile](./Makefile) in that folder
2. Change [Makefile](./Makefile)  for new module name
3. The file is [input.txt](./txtfile/input.txt) and you have to save the content to output.txt in a reversed order

## Rivision history : v1.0
