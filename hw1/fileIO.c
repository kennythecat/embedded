#include <linux/module.h>
#include <linux/init.h>
#include <linux/fs.h>
#include <linux/uaccess.h>
static char buf_i[300];
static char buf_o[300];
static int IO_init(void)
{
    int i = 0, j= 0, k =0;
    struct file *fp_i, *fp_o;
    loff_t pos = 0;

    printk("readfile enter!\n");
    fp_i = filp_open("txtfile/input.txt", O_RDWR,0);
    if (IS_ERR(fp_i)) {
        printk("create file error\n");
        return -1;
    }
    kernel_read(fp_i, buf_i, sizeof(buf_i), &pos); 
    printk("read: %s\n", buf_i);
    while(buf_i[i]!='\0') i++;

    for(j = i-1; j >= 0; j--)
    {
        buf_o[k] = buf_i[j];
        k++;
    }
    printk("output: %s\n", buf_o);
    fp_o = filp_open("output.txt",O_RDWR,0);
    if (IS_ERR(fp_o)) 
    {
        printk("create file error\n");
        return -1;
    }
    pos = 0;
    kernel_write(fp_o, buf_o, sizeof(buf_o), &pos);
    filp_close(fp_i, NULL);
    filp_close(fp_o, NULL);
    return 0;
}
static void IO_exit(void)
{
    printk(KERN_INFO "IO  Exit!\n");
}
module_init(IO_init);
module_exit(IO_exit);
MODULE_LICENSE("GPL");