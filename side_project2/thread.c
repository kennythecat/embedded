#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/slab.h>                 //kmalloc()
#include<linux/uaccess.h>              //copy_to/from_user()
#include <linux/kthread.h>             //kernel threads
#include <linux/sched.h>               //task_struct 
#include <linux/delay.h>
#include <linux/err.h>
 
static int __init demo_driver_init(void);
static void __exit demo_driver_exit(void);
 
static struct task_struct *demo_thread;
 
int thread_function(void *pv);

int thread_function(void *pv)
{
    int i=0;
    while(!kthread_should_stop()) {
        pr_info("In demo Thread Function %d\n", i++);
        msleep(1000);
    }
    return 0;
}

static int __init demo_driver_init(void)
{
        demo_thread = kthread_create(thread_function,NULL,"demo Thread");
        if(demo_thread) {
            wake_up_process(demo_thread);
        } 
        else {
            pr_err("Cannot create kthread\n");
            goto r_device;
        }

        return 0;
 
}
 
static void __exit demo_driver_exit(void)
{
        kthread_stop(demo_thread);
}
 
module_init(demo_driver_init);
module_exit(demo_driver_exit);
 
MODULE_LICENSE("GPL");
