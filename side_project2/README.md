# Side Project 2 - Synchronization and Threading
## Rivision history : Initial

## Table of Contents
- [Side Project 2 - Synchronization and Threading](#side-project-2---synchronization-and-threading)
  - [Rivision history : Initial](#rivision-history--initial)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
    - [Interrupt](#interrupt)
    - [Kernel Thread](#kernel-thread)
    - [Work Queue](#work-queue)
    - [Tasklet](#tasklet)
    - [Wait Queue](#wait-queue)
  - [Conclusion Table](#conclusion-table)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

There differents way to Synchronization and Threading:
  - [Interrupt](./interrupt.c)
  - [Kernel Thread](./thread.c)
  - [Workqueue ](./workqueue.c)
  - [Tasklet](./tasklet.c)
  - [Wait queue](./waitqueue.c)


### Interrupt

  Interrupts are low-level mechanisms that CPUs use to handle asynchronous events. When a hardware event occurs, an interrupt signal alerts the CPU to pause its current operations and execute a specific function to handle this event, known as the interrupt handler. In the Linux kernel, you can register and unregister interrupt handlers for specific interrupt lines.

  Registering an Interrupt handler for IRQ 
  
  ```
  #define IRQ_NO 11
  ...

  static irqreturn_t irq_handler(int irq,void *dev_id) {
    printk(KERN_INFO "Shared IRQ: Interrupt Occurred");
    return IRQ_HANDLED;
  }
  ...

  if (request_irq(IRQ_NO, irq_handler, IRQF_SHARED, "demo_device", (void *)(irq_handler))) {
            printk(KERN_INFO "my_device: cannot register IRQ ");
                    goto irq;
        }
  ```

  In `demo_read()`
  
  ```
  struct irq_desc *desc;

  printk(KERN_INFO "Read function\n");
  desc = irq_to_desc(11);
  if (!desc) 
  {
      return -EINVAL;
  }
  __this_cpu_write(vector_irq[59], desc);
  asm("int $0x3B");  // Corresponding to irq 11
  return 0;
  ```

### Kernel Thread

  A kernel thread is like a regular user-space thread but runs in kernel-space. Kernel threads are scheduled by the Linux scheduler and can sleep, wake-up, and get preempted just like user-space threads.

  Creae and Wake up

  ```
   demo_thread = kthread_create(thread_function,NULL,"demo Thread");
        if(demo_thread) {
            wake_up_process(demo_thread);
        } 
        else {
            pr_err("Cannot create kthread\n");
            goto r_device;
        }
  ```

  Sleep

  ```
  while(!kthread_should_stop()) {
        pr_info("In demo Thread Function %d\n", i++);
        msleep(1000);
    }
  ```

### Work Queue

  Workqueues are used to schedule functions to be run in the context of a pre-existing kernel thread. Workqueues ensure that the scheduled function (work) gets executed by a worker thread at some point in the future.

  Allocate the work to the queue

  ```
  static irqreturn_t irq_handler(int irq,void *dev_id) {
          printk(KERN_INFO "Shared IRQ: Interrupt Occurred\n");
          /*Allocating work to queue*/
          queue_work(own_workqueue, &work);
          
          return IRQ_HANDLED;
  }
  ```
### Tasklet

  Tasklets are a type of "softirq" in the Linux kernel. They are used for tasks that are relatively quick but can be deferred. Unlike workqueues, they run in interrupt context and therefore cannot sleep or perform blocking operations.

  Dynamically Creation of Tasklet

  ```
  struct tasklet_struct *tasklet;
  tasklet  = kmalloc(sizeof(struct tasklet_struct),GFP_KERNEL);
  if(tasklet == NULL) {
    printk(KERN_INFO "etx_device: cannot allocate Memory");
  }
  tasklet_init(tasklet,tasklet_fn,0);
  ```

### Wait Queue
  Wait queues are used to put a task to sleep until some condition gets satisfied. A task can add itself to a wait queue and then go to sleep. Another part of the code (possibly an interrupt handler) can wake up one or all tasks in a wait queue when some condition becomes true.

  `wait_function()`
  
  ```
  while(1) {
                pr_info("Waiting For Event...\n");
                wait_event_interruptible(wait_queue_demo, wait_queue_flag != 0 );
                if(wait_queue_flag == 2) {
                        pr_info("Event Came From Exit Function\n");
                        return 0;
                }
                pr_info("Event Came From Read Function - %d\n", ++read_count);
                wait_queue_flag = 0;
        }
        do_exit(0);
        return 0;
  ```
  
## Conclusion Table

| Attribute                | Interrupt              | Kernel Thread          | Workqueue              | Tasklet                | Wait Queue             |
|--------------------------|------------------------|------------------------|------------------------|------------------------|------------------------|
| Context                  | Interrupt              | Process                | Process                | Interrupt              | Process/Interrupt      |
| Can Sleep                | No                     | Yes                    | Yes                    | No                     | Yes/No                 |
| Latency                  | Very Low               | Medium to High         | Medium to High         | Low                    | Medium to High         |
| Preemptibility           | Non-Preemptible        | Preemptible            | Preemptible            | Non-Preemptible        | Preemptible            |
| Scheduling               | Immediate              | Kernel Scheduler       | Kernel Scheduler       | SoftIRQ                | Conditional            |
| Typical Use-Case         | Immediate HW events    | Background tasks       | Deferred execution     | Quick, non-urgent tasks| Wait for condition     |
| Blocking Operations      | No                     | Yes                    | Yes                    | No                     | Yes/No                 |
| Priority                 | Highest                | Configurable           | Configurable           | High (lower than Interrupt)| Configurable       |
| CPU Utilization          | High (if frequent)     | Variable               | Variable               | Moderate               | Low                    |
| Can be deferred          | No                     | N/A                    | Yes                    | Yes                    | N/A                    |

Here's a brief explanation of each attribute:

- **Context**: Where the task runs. Interrupt tasks run in interrupt context, others mostly in process context.
  
- **Can Sleep**: Whether the task can go to sleep (block) or perform operations that might sleep.
  
- **Latency**: The speed at which tasks get executed. Lower latency means the task is executed quicker.
  
- **Preemptibility**: Whether the task can be preempted by other tasks or not.
  
- **Scheduling**: How the task gets scheduled to run.
  
- **Typical Use-Case**: Most common scenarios where these tasks are used.
  
- **Blocking Operations**: Whether the construct allows for blocking operations, like disk I/O.
  
- **Priority**: Relative priority against other constructs.
  
- **CPU Utilization**: The relative CPU utilization when using each construct.
  
- **Can be deferred**: Whether execution can be postponed to a later time or not.

Note: The attributes like Latency, Priority, and CPU Utilization are relative and can vary based on the specific implementation and workload.

## Rivision history : v1.0
