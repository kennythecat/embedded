# Side Project 4 - Event and Signal Handling

## Table of Contents
- [Side Project 4 - Event and Signal Handling](#side-project-4---event-and-signal-handling)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
    - [Signals](#signals)
    - [Timer](#timer)
    - [Completion](#completion)
  - [Conclusion Table](#conclusion-table)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

There are differents way of Event and Signal Handling
  - [Signals](./signals.c)
  - [Timer](./timer.c)
  - [Complettion ](./completion.c)

### Signals

  - Asynchronous notification sent to a process or thread.
  - Can be sent from user-space to kernel or between user-space processes.
  - Generally used for IPC, termination, and other forms of event notification.

  Sending Signal

  ```
  memset(&info, 0, sizeof(struct siginfo));
  info.si_signo = SIGdemo;
  info.si_code = SI_QUEUE;
  info.si_int = 1;

  if (task != NULL) {
        printk(KERN_INFO "Sending signal to app\n");
        if(send_sig_info(SIGdemo, &info, task) < 0) {
            printk(KERN_INFO "Unable to send signal\n");
        }
    }
  ```

### Timer

  - Executes a function at a specific time or after a time period has elapsed.
  - Can be used in user-space for things like timeouts, or in the kernel for scheduling tasks.

  Setup timer 

  ```
  timer_setup(&demo_timer, timer_callback, 0);
  mod_timer(&demo_timer, jiffies + msecs_to_jiffies(TIMEOUT));
  ```
  

### Completion

  - Kernel synchronization mechanism.
  Allows tasks to wait for some condition to complete.
  - Particularly useful in scenarios like waiting for hardware to become ready.

  Completion Structure

  ```
  DECLARE_COMPLETION(data_read_done);
 
  dev_t dev = 0;
  static struct class *dev_class;
  static struct cdev demo_cdev;
  int completion_flag = 0;
  ```

  Wait for Completion

  ```
  wait_for_completion (&data_read_done);
  ```

  Done Completion

  ```
  completion_done (&data_read_done)
  ```


## Conclusion Table

| Attribute                 | Signals          | Timer              | Completion         |
|---------------------------|------------------|--------------------|--------------------|
| Context                   | User/Kernel      | User/Kernel        | Kernel             |
| Can Sleep                 | Yes              | Yes                | Yes                |
| Synchronicity             | Asynchronous     | Asynchronous       | Synchronous        |
| Latency                   | Variable         | Scheduled          | Immediate after completion  |
| Complexity                | Moderate         | Low                | Low                |
| Typical Use-Case          | IPC, Event Notification | Timing, Delays  | Synchronization between tasks |
| Blocking Operations       | Yes              | Yes                | Yes                |
| Resource Utilization      | Moderate         | Low                | Low                |
| Reusability               | Yes              | Yes                | Yes but must be reinitialized  |
| Condition-based Trigger   | Yes              | No                 | No                 |
| Preemptibility            | Preemptible      | Preemptible        | Preemptible        |

Here is a brief explanation of each attribute:

- **Context**: The execution context where the mechanism can be used, either in user-space, kernel-space, or both.
  
- **Can Sleep**: Indicates if the task can go to sleep while waiting for the event or signal.
  
- **Synchronicity**: Specifies whether the mechanism operates synchronously or asynchronously with respect to the triggering event.
  
- **Latency**: The speed at which tasks get executed or events get handled after they are triggered.
  
- **Complexity**: The relative ease or difficulty of using the mechanism.
  
- **Typical Use-Case**: Describes typical scenarios where each method is useful.
  
- **Blocking Operations**: Specifies whether or not you can perform blocking operations while waiting for the event or signal.
  
- **Resource Utilization**: Indicates how resource-intensive each method is, including CPU and memory overhead.
  
- **Reusability**: Specifies if the mechanism can be reused once triggered or completed.
  
- **Condition-based Trigger**: Specifies whether the mechanism can be triggered based on a condition or not.
  
- **Preemptibility**: Indicates if the task can be preempted while waiting for the event or signal.

## Rivision history : v1.0
