#include <linux/kernel.h>
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kdev_t.h>
#include <linux/fs.h>
#include <linux/cdev.h>
#include <linux/device.h>
#include<linux/slab.h>                 //kmalloc()
#include<linux/uaccess.h>              //copy_to/from_user()
#include <linux/kthread.h>             //kernel threads
#include <linux/sched.h>               //task_struct 
#include <linux/delay.h>
#include <linux/seqlock.h>
#include <linux/err.h>
//Seqlock variable
seqlock_t demo_seq_lock;
 
unsigned long demo_global_variable = 0;
dev_t dev = 0;
static struct class *dev_class;
static struct cdev demo_cdev;
 
static int __init demo_driver_init(void);
static void __exit demo_driver_exit(void);
 
static struct task_struct *demo_thread1;
static struct task_struct *demo_thread2; 
 
static int demo_open(struct inode *inode, struct file *file);
static int demo_release(struct inode *inode, struct file *file);
static ssize_t demo_read(struct file *filp, char __user *buf, size_t len,loff_t * off);
static ssize_t demo_write(struct file *filp, const char *buf, size_t len, loff_t * off);
 
int thread_function1(void *pv);
int thread_function2(void *pv);
 
//Thread used for writing
int thread_function1(void *pv)
{
    while(!kthread_should_stop()) {  
        write_seqlock(&demo_seq_lock);
        demo_global_variable++;
        write_sequnlock(&demo_seq_lock);
        msleep(1000);
    }
    return 0;
}
 
//Thread used for reading
int thread_function2(void *pv)
{
    unsigned int seq_no;
    unsigned long read_value;
    while(!kthread_should_stop()) {
        do {
            seq_no = read_seqbegin(&demo_seq_lock);
        read_value = demo_global_variable;
    } while (read_seqretry(&demo_seq_lock, seq_no));
        pr_info("In demo Thread Function2 : Read value %lu\n", read_value);
        msleep(1000);
    }
    return 0;
}

//File operation structure 
static struct file_operations fops =
{
        .owner          = THIS_MODULE,
        .read           = demo_read,
        .write          = demo_write,
        .open           = demo_open,
        .release        = demo_release,
};

static int demo_open(struct inode *inode, struct file *file)
{
        pr_info("Device File Opened...!!!\n");
        return 0;
}

static int demo_release(struct inode *inode, struct file *file)
{
        pr_info("Device File Closed...!!!\n");
        return 0;
}

static ssize_t demo_read(struct file *filp, 
                char __user *buf, size_t len, loff_t *off)
{
        pr_info("Read function\n");
 
        return 0;
}

static ssize_t demo_write(struct file *filp, const char __user *buf, size_t len, loff_t *off)
{
        pr_info("Write Function\n");
        return len;
}

static int __init demo_driver_init(void)
{
        /*Allocating Major number*/
        if((alloc_chrdev_region(&dev, 0, 1, "demo_Dev")) <0){
                pr_info("Cannot allocate major number\n");
                return -1;
        }
        pr_info("Major = %d Minor = %d \n",MAJOR(dev), MINOR(dev));
 
        /*Creating cdev structure*/
        cdev_init(&demo_cdev,&fops);
 
        /*Adding character device to the system*/
        if((cdev_add(&demo_cdev,dev,1)) < 0){
            pr_info("Cannot add the device to the system\n");
            goto r_class;
        }
 
        /*Creating struct class*/
        if(IS_ERR(dev_class = class_create(THIS_MODULE,"demo_class"))){
            pr_info("Cannot create the struct class\n");
            goto r_class;
        }
 
        /*Creating device*/
        if(IS_ERR(device_create(dev_class,NULL,dev,NULL,"demo_device"))){
            pr_info("Cannot create the Device \n");
            goto r_device;
        }
 
        
        /* Creating Thread 1 */
        demo_thread1 = kthread_run(thread_function1,NULL,"demo Thread1");
        if(demo_thread1) {
            pr_err("Kthread1 Created Successfully...\n");
        } else {
            pr_err("Cannot create kthread1\n");
             goto r_device;
        }
 
         /* Creating Thread 2 */
        demo_thread2 = kthread_run(thread_function2,NULL,"demo Thread2");
        if(demo_thread2) {
            pr_err("Kthread2 Created Successfully...\n");
        } else {
            pr_err("Cannot create kthread2\n");
             goto r_device;
        }
 
        //Initialize the seqlock
        seqlock_init(&demo_seq_lock);
        
        pr_info("Device Driver Insert...Done!!!\n");
        return 0;

r_device:
        class_destroy(dev_class);
r_class:
        unregister_chrdev_region(dev,1);
        cdev_del(&demo_cdev);
        return -1;
}

static void __exit demo_driver_exit(void)
{
        kthread_stop(demo_thread1);
        kthread_stop(demo_thread2);
        device_destroy(dev_class,dev);
        class_destroy(dev_class);
        cdev_del(&demo_cdev);
        unregister_chrdev_region(dev, 1);
        pr_info("Device Driver Remove...Done!!\n");
}
 
module_init(demo_driver_init);
module_exit(demo_driver_exit);
 
MODULE_LICENSE("GPL");
