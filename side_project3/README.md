# Side Project 3 - Synchronization Mechanisms

## Table of Contents
- [Side Project 3 - Synchronization Mechanisms](#side-project-3---synchronization-mechanisms)
  - [Table of Contents](#table-of-contents)
  - [Description](#description)
    - [Mutex](#mutex)
    - [Spinlock](#spinlock)
    - [Seqlock](#seqlock)
    - [Atomic Operations](#atomic-operations)
  - [Conclusion](#conclusion)
  - [Rivision history : v1.0](#rivision-history--v10)

***
## Description

There are differents Synchronization Mechanisms:
  - [Mutex](./mutex.c)
  - [Spinlock](./spinlock.c)
  - [Seqlock ](./seqlock.c)
  - [Atomic](./atomic.c)

### Mutex

  - General-purpose mutual exclusion lock.
  - Can sleep, so shouldn't be used in interrupt context.
  - Handles priority inversion.
  
  Initializing Mutex Dynamically 

  ```
  struct mutex demo_mutex; 
  mutex_init(&demo_mutex);
  ```

### Spinlock

  - Used for short critical sections where sleeping is not acceptable.
  - Busy-waits (spins) until lock is acquired.
  - Unsuitable for long critical sections.

  Initializing Read Write Spinlock 

  ```
  static DEFINE_RWLOCK(demo_rwlock); 
  mutex_init(&demo_mutex);
  ```

### Seqlock

  - Optimized for read-mostly scenarios.
  - Writers will invalidate ongoing readers, but readers do not block writers.
  - Busy-waits for readers if a writer is in progress.

  Initializing Read Write Seqlock 

  ```
  #include <linux/seqlock.h>
  demo_seq_lock
  seqlock_init(&demo_seq_lock);
  ```

  In the thread

  ```
  unsigned int seq_no;
    unsigned long read_value;
    while(!kthread_should_stop()) {
        do {
            seq_no = read_seqbegin(&demo_seq_lock);
        read_value = demo_global_variable;
    } while (read_seqretry(&demo_seq_lock, seq_no));
        pr_info("In demo Thread Function2 : Read value %lu\n", read_value);
        msleep(1000);
    }
    return 0;
  ```

### Atomic Operations

  - Operations that complete in a single step and cannot be interrupted.
  - Useful for very simple flags, counters, and state changes.
  - Usually implemented using hardware-specific instructions.

  Atomic integer variable

  ```
  atomic_t demo_global_variable = ATOMIC_INIT(0);
  ```

  Atomically Increments (+1)

  ```
  atomic_inc(&demo_global_variable);
  ```

  Atomically Read

  ```
  atomic_read(&demo_global_variable);
  ```

## Conclusion

| Attribute               | Mutex          | Spinlock       | Seqlock         | Atomic Operations   |
|-------------------------|----------------|----------------|-----------------|---------------------|
| Context                 | Process        | Any            | Any             | Any                 |
| Can Sleep               | Yes            | No             | No              | No                  |
| CPU Utilization         | Low            | High           | Moderate        | Low                 |
| Complexity              | Moderate       | Low            | Moderate        | Low                 |
| Priority Inversion      | Managed        | Not managed    | Not managed     | N/A                 |
| Reader-Writer Variant   | Yes            | Yes            | Yes             | No                  |
| Typical Use-Case        | General Purpose| Short Critical Sections | Read-mostly sections | Simple counters, flags  |
| Blocking Operations     | Yes            | No             | No              | No                  |

Here's a brief explanation of each attribute:

- **Context**: Indicates where this mechanism can be safely used. "Any" means both in process and interrupt contexts.
  
- **Can Sleep**: Specifies whether acquiring the synchronization primitive can put the task to sleep.
  
- **CPU Utilization**: Relative CPU usage when waiting to acquire the lock. Spinlocks consume more CPU when spinning, while mutexes and atomics are more efficient.
  
- **Complexity**: The relative complexity of using the synchronization mechanism. Mutexes, for example, require proper handling to avoid deadlocks.
  
- **Priority Inversion**: Whether the mechanism has built-in management of priority inversion problems. Mutexes in Linux often have priority inheritance to deal with this issue.
  
- **Reader-Writer Variant**: Indicates if the mechanism has a separate variant that allows multiple readers but only one writer, useful in read-mostly scenarios.
  
- **Typical Use-Case**: Common situations where each mechanism is most useful.
  
- **Blocking Operations**: Indicates whether or not you can perform blocking operations (like waiting for I/O to complete) while holding the synchronization primitive.


Each synchronization mechanism is best suited for specific kinds of problems, so the appropriate one should be chosen based on the specific requirements of the task at hand.

## Rivision history : v1.0
