# Embedded System

## Table of Contents
- [hw1](./hw1) - Read/Write In Linux Kernel
- [hw2](./hw2) - Extend kernel module into a character driver​
- [side project1](./side_project1) - Communication Interfaces
- [side project2](./side_project2) - Synchronization and Threading
- [side project3](./side_project3) - Synchronization Mechanisms
- [side project4](./side_project4) - Event and Signal Handling

***

## Description

This repository contains the following assignments :

- `hw1` 
  
  Read/Write Files In Linux Kernel, and compile the write the kernel module

- `hw2`

  Write a character driver and a C based program to access the driver

- `SP1 - Communication Interfaces`
 
  Implement differents methods to communicate between the Userspace and Kernel Space

  - `IOCTL`
    
    Low-Level Hardware Access, Input/Output Control

  - `Procfs`

    The procfs is used to export the process-specific information.

  - `Sysfs`

    Sysfs is the commonly used method to export system information. 

- `SP2 - Synchronization and Threading`
  
  Implement differents way to Synchronization and Threading

  - `Interrupt`
  
    Interrupts are low-level mechanisms that CPUs use to handle asynchronous events

  - `Thread`
  
    Kernel threads are scheduled by the Linux scheduler and can sleep, wake-up, and get preempted.

  - `Workqueue`

    Workqueues are used to schedule functions to be run in the context of a pre-existing kernel thread

  - `Tasklet`
  
    Tasklet are used for tasks that are relatively quick but can be deferred.

  - `Wait queue`

    Wait queues are used to put a task to sleep until some condition gets satisfied.

- `SP3 - Synchronization Mechanisms`

  - `Mutex`
  
    General-purpose mutual exclusion lock

  - `Spinlock`
  
    Busy-waits (spins) until lock is acquired

  - `Seqlock`

    Busy-waits for readers if a writer is in progress, optimized for read-mostly scenarios

  - `Atomic`

    Operations that complete in a single step and cannot be interrupted

- `SP4 - Event and Signal Handling`

  - `Signals`

    Asynchronous notification sent to a process or thread

  - `Timer`

    Executes a function at a specific time or after a time period has elapse

  - `Complettion`

    Allows tasks to wait for some condition to complete

## Rivision history : v1.0
